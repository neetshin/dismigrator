import { JSDOM } from "jsdom";
import fetch from "node-fetch";

export interface Sticker {
  type: string;
  id: string;
  staticUrl: string;
  fallbackStaticUrl: string;
  animationUrl: string;
  popupUrl: string;
  soundUrl: string;
}

export const getStaticUrlFromId = (id: string) =>
  `https://stickershop.line-scdn.net/stickershop/v1/sticker/${id}/iPhone/sticker@2x.png`;

export class LINEPackage {
  private constructor(
    public readonly id: string,
    public readonly jsdom: JSDOM
  ) {}

  static async init(id: string) {
    const jsdom = await fetch(`https://store.line.me/stickershop/product/${id}`)
      .then(response => response.text())
      .then(text => new JSDOM(text));

    return new LINEPackage(id, jsdom);
  }

  get stickers(): Sticker[] {
    const { document } = this.jsdom.window;

    return Array.from(document.querySelectorAll(".mdCMN09Li"))
      .map(
        element =>
          JSON.parse(element.getAttribute("data-preview") || "") as Sticker
      )
      .filter((item): item is Sticker => item !== undefined);
  }
}
