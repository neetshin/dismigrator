import fetch from "node-fetch";
import Discord, { Message } from "discord.js";
import dotenv from "dotenv";
import { fromEvent } from "rxjs";
import { filter } from "rxjs/operators";
import { LINEPackage, getStaticUrlFromId } from "./utils";
import path from "path";
import sharp from "sharp";

dotenv.config({ path: path.resolve(__dirname, "../.env") });

const discordToken = process.env.DISCORD_TOKEN as string;
const client = new Discord.Client();
client.login(discordToken);

const message$ = fromEvent<Message>(client, "message");
const customEmojiRegexp = /<:dism(?<id>[0-9]+):[0-9]+>/;

fromEvent(client, "ready").subscribe(() => {
  console.log("logged in");
});

message$
  .pipe(filter(message => message.content.startsWith("/migrate")))
  .subscribe(async message => {
    message.channel.startTyping();

    const match = /\/migrate\s?(?<packageId>.+)/.exec(message.content);
    if (!(match && match.groups && match.groups.packageId)) return;

    const { packageId } = match.groups;
    const { stickers } = await LINEPackage.init(packageId);

    for (const sticker of stickers) {
      const { id, staticUrl } = sticker;

      const image = await fetch(staticUrl)
        .then(response => response.arrayBuffer())
        .then(arrayBuffer => Buffer.from(arrayBuffer));

      const compressedImage = await sharp(image)
        .resize(50)
        .toBuffer();

      await message.guild.createEmoji(compressedImage, `dism${id}`);
    }

    message.channel.stopTyping(true);
    message.channel.send(
      `${stickers.length} stickers have just been migrated.`
    );
  });

message$
  .pipe(filter(message => customEmojiRegexp.test(message.content)))
  .subscribe(message => {
    const matches = customEmojiRegexp.exec(message.content);

    if (!(matches && matches.groups && matches.groups.id)) {
      return;
    }

    const { id: stickerId } = matches.groups;
    message.channel.send(getStaticUrlFromId(stickerId));
  });
